package com.example.lecture19.Activityes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lecture19.Adapters.Adapter
import com.example.lecture19.Api.RequestApi
import com.example.lecture19.Interfaces.CallbackApi
import com.example.lecture19.Models.MyModel
import com.example.lecture19.R
import kotlinx.android.synthetic.main.activity_list_recycle.*
import org.json.JSONObject

class listRecycle : AppCompatActivity() {
    var model: MyModel = MyModel()
    var admodel:MyModel.ad=MyModel.ad()
    var datalist= mutableListOf<MyModel.data>()
    var adlist= mutableListOf<MyModel.ad>()
    lateinit var  adapter:Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_recycle)

        init()
    }
    private  fun  init(){

         RequestApi.getRequest("users",object : CallbackApi {
             override fun onResponse(value: String?) {
                  parseJson(value)
             }

             override fun onFailure(value: String?) {
                 d("fff",value)
             }
         })


    }

    private fun parseJson(jsonstring:String?){




        val json= JSONObject(jsonstring)
        if(jsoncheck(json,"page")){
            model.page=json.getInt("page")

        }
        if(jsoncheck(json,"per_page")) {
            model.perPage=json.getInt("per_page")
            d("perpage",model.perPage.toString())
        }
        if(jsoncheck(json,"total")) {
            model.total=json.getInt("total")

        }
        if(jsoncheck(json,"total_pages")) {
            model.totalPages=json.getInt("total_pages")
        }
        if(jsoncheck(json,"data")) {
            var array= json.getJSONArray("data")


            (0 until array.length()).forEach(){
                    i->
                var datajs=array.getJSONObject(i)
                datalist.add(MyModel.data(datajs.getInt("id"),datajs.getString("email"),datajs.getString("first_name"),datajs.getString("last_name"),datajs.getString("avatar")))
                d("sds","asas")
            }
        }

        if(jsoncheck(json,"ad")){
            var adjs=json.getJSONObject("ad")
            admodel.company=adjs.getString("company")
            admodel.text=adjs.getString("text")
            admodel.url=adjs.getString("url")
            adlist.add(admodel)
            model.adlist=adlist
            d("ddd",admodel.url)
        }


        adapter= Adapter(datalist)
        recycle.layoutManager= LinearLayoutManager(this)
        recycle.adapter=adapter



    }
    fun jsoncheck(jsonobject:JSONObject,key:String):Boolean{

        if(!jsonobject.has(key)) return false
        return true

    }

}
