package com.example.lecture19.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.lecture19.Models.MyModel
import com.example.lecture19.R
import kotlinx.android.synthetic.main.list_view.view.*

class Adapter(val mylist:MutableList<MyModel.data>): RecyclerView.Adapter<Adapter.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        return holder(LayoutInflater.from(parent.context).inflate(R.layout.list_view,parent,false))
    }

    override fun getItemCount(): Int {
        return mylist.size
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.onbind()
    }

    inner class holder(itemView: View): RecyclerView.ViewHolder(itemView){
        private  lateinit var model: MyModel.data

        fun onbind(){
            model=mylist[adapterPosition]
            itemView.firstname.text=model.FirstName
            itemView.lastname.text=model.LastName
            itemView.email.text=model.email

            Glide.with(itemView.context).load(model.avatar).into(itemView.image)

        }


    }
}