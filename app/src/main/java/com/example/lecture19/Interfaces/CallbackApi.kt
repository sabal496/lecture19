package com.example.lecture19.Interfaces

interface CallbackApi {
    fun onResponse(value:String?)
    fun onFailure(value:String?)
}